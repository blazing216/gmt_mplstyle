# Style sheet for matplotlib to have an appearance of GMT
## How to use
1. Copy `gmt.mplstyle` to `~/.config/matplotlib/stylelib/`
2. Add `plt.style.use('gmt')` before plotting.